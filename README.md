# ECLSAv2

The ECLSAv2 project it is the second major implementation of the Erasure Coding Link Service Adapter (ECLSA)
It is part of the Unibo-DTN "umbrella" project that aims to implement the most important DTN protocols.
Give a glance to https://gitlab.com/unibo-dtn/ to see our ancillary projects.

## Requirements

The ECLSA core requires an external implementation of PL-FEC (Packet Layering Forward Erasure Codes);
the package contains:

- an interface for OpenFEC (and also a copy of it for user convenience);
- an interface to DLR-FEC (but not the DLR-FEC code itself, which may be asked directly to DLR);
- a dummy interface to be used as a template for future extensions

## Download, compilation and installation

### Required steps

You can either clone the repository

```bash
git clone https://gitlab.com/unibo-dtn/eclsa.git
```

or download the .tar.gz file from it.

Enter the local directory

```bash
cd eclsa
```

Run the configure step as follows for default building, otherwise read the configure override section below.

```bash
./configure
```

Build and install as usual.

```bash
make
sudo make install
sudo ldconfig
```

#### Default building

By default, eclsa (i.e. both eclso and eclsi executables) is built with support for:

- Upper layer: UniboLTP
- Lower layer: UDP
- FEC codes: OpenFEC
- debug: OFF

#### Configure overrides

- Upper layer: ION-LTP (BPv7); add `--with-ion-path=<path to ION>`
- Upper layer: ION-LTP (BPv6); add `--with-ion-path=<path to ION> --enable-bpv6`
- FEC codes: DLR; add `--with-codecLibecDLR` (they are however not included in the standard package; see licence
  documentation)
- FEC codes: dummy; add `--with-codecDummyDEC` (a stub to be used as a template for future extensions)
- debug: ON; add --enable-debug

#### Use with UniboLTP

ECLSA has been originally designed to work as an ION extension, below ION-LTP, and it can still be used this way (see
USe with ION, below).
However, after the development of Unibo-LTP, its primary intended use is below Unibo-LTP. To this end:

- Build ECLSA for UniboLTP (see above) and install it.
- Install and configure UniboLTP as usual
- In the `spans.json` file used by UniboLTP insert proper ECLSA configuration settings; see the template in the UniboLTP
  package
- Be aware that ECLSA use requires a good level of understanding of both PL-FEC theory and ECLSA design (see
  Documentation).

Note that as UniboLTP is in turn compatible with both ION-BP and Unibo-BP; the following three protocol stacks are
possible

- Unibo-BP -> UniboLTP -> ECLSA
- ION-BP -> UniboLTP -> ECLSA
- ION-BP -> ION-LTP -> ECLSA

As far as ECLSA configuration is concerned there are no differences, but for HSLTP [2]. The HSLTP mode is obtained
by setting the HSLTP option of ECLSA and:

- in ION LTP, by using LTP red sessions (ION default);
- in UniboLTP, by using LTP Orange sessions (experimental). This because Orange sessions
  allowed us to greatly simplify HSLTP code in the UniboLTP version.
  For the benefits of Orange plus ECLSA see [3].

#### Use with ION-LTP

ECLSA has been originally designed to work as an ION extension and can still be used as that, below ION-LTP. To this
end:

- Build ECLSA for ION (see above) and install it;
- Install and configure ION as usual
- In your `ltpadmin` configuration replace the `udplsi` and `udplso` strings with the corresponding `eclsi` and `eclso`
  ones; see
  vm1 and vm2 templates provided in the code (`vm1ECLSA.rc` and `vm2ECLSA.rc` in the config_files directory).
- Be aware that ECLSA use requires a good level of understanding of both PL-FEC theory and ECLSA design (see
  Documentation).

## Documentation

ECLSAv2 is inspired by (but it is not a compliant implementation of) what is
described in the informative document CCSDS 131.5-O-1 Orange Book, Sept. 2014
ERASURE CORRECTING CODES FOR USE IN NEAR-EARTH AND DEEP-SPACE COMMUNICATIONS.

A comprehensive description of ECLSAv2 and of the HSLTP extension can be found in:

[1] N. Alessi, C. Caini, T. de Cola and M. Raminella, "Packet Layer Erasure Coding in
Interplanetary Links: The LTP Erasure Coding Link Service Adapter,"
in IEEE Transactions on Aerospace and Electronic Systems, vol. 56, no. 1,
pp. 403-414, Feb. 2020,
DOI: 10.1109/TAES.2019.2916271. <https://doi.org/10.1109/TAES.2019.2916271>

[2] N. Alessi, C. Caini, A. Ciliberti and T. de Cola, "HSLTP: An LTP Variant for
High-Speed Links and Memory Constrained Nodes," in IEEE Transactions on Aerospace and
Electronic Systems, vol. 56, no. 4, pp. 2922-2933, Aug. 2020,
DOI: 10.1109/TAES.2019.2958190. <https://doi.org/10.1109/TAES.2019.2958190>

The benefits of the joint use of ECLSA and the experimental Orange LTP color are presented in:

[3] A. Bisacchi, C. Caini and T. de Cola, "Multicolor Licklider Transmission Protocol: An LTP Version for Future
Interplanetary Links,"
in IEEE Transactions on Aerospace and Electronic Systems, vol. 58, no. 5, pp. 3859-3869, Oct. 2022,
doi: 10.1109/TAES.2022.3176847. (Open Source) <https://doi.org/10.1109/TAES.2022.3176847>

## Credits

- Carlo Caini: supervisor (carlo.caini@unibo.it)
- Nicola Alessi: main author (nicola.alessi@studio.unibo.it)
- Andrea Bisacchi: main coauthor (andreabisacchi@gmail.com)
- Marco Raminella: author of the OpenFEC adapter (marco.raminella@studio.unibo.it)
- Azzurra Ciliberti: author of the HSLTP extension (azzurra.ciliberti@studio.unibo.it)
- Andrea Lo Buglio: author of new `configure.ac` and `Makefiles.am` files; extension to IPv6 (
  andrea.lobuglio@studio.unibo.it)
- Andra Grano: author of new ECLSA packet serialization (andrea.grano@studio.unibo.it)

# Licences

Note that ECLSA components have been written by different authors belonging to different entities, and released
under different licenses. In particular:

1. `contrib/ECLSAv2/ec/eclsa`

The software included in this directory is the core of ECLSAv2 (ECLSA version
2), the copyright is of the University of Bologna 2016, and it is released as
free software under GPL v2 and later versions. ECLSAv2 core can be interfaced
with different codecs, using different FECs, developed by other entities (see
below).

2. `contrib/ECLSAv2/ec/libec`

This directory is usually empty; it is included to insert the
FEC codec developed by DLR (German Aerospace Center) in the right place.
The copyright is of DLR and it is NOT released as free software. Contact DLR for
further details on code and licence.

3. `contrib/ECLSAv2/ec/openFEC`

The software included in this directory contains the FEC codec developed by
the OpenFEC project (core contributors from the French institutions INRIA and
ISAE). It is free software consisting of different parts governed by several
licenses. See http://openfec.org/accueil.html

For further information and license details, see documentation inside the
cited directories.
