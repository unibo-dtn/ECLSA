/*
    network_utils.h

     Author: Andrea Lo Buglio (andrea.lobuglio@studio.unibo.it)
     Author: Andrea Bisacchi (andreabisacchi@gmail.com)
     Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

    Copyright (c) 2024, Alma Mater Studiorum, University of Bologna
    All rights reserved.

*/

#ifndef ECLSA_NETWORK_UTILS_H
#define ECLSA_NETWORK_UTILS_H
#include <netdb.h>

typedef struct {
	int ai_family; // if == AF_INET => IPv4, if == AF_INET6 => IPv6
	union {
		struct sockaddr_in ipv4;
		struct sockaddr_in6 ipv6;
	} sock_address;
} INET_struct;

int parseHostnameAndPort(char* hostnameAndPort, INET_struct *addr, uint16_t defaultPort);

#endif //ECLSA_NETWORK_UTILS_H
