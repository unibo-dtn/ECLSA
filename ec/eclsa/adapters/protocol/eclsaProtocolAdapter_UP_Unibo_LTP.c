/*
eclsaProtocolAdapter_UP_UniboLTP.c

 Author: Andrea Bisacchi (andrea.bisacchi5@studio.unibo.it)
 Project Supervisor: Carlo Caini (carlo.caini@unibo.it)

Copyright (c) 2016, Alma Mater Studiorum, University of Bologna
 All rights reserved.

 * */

/*
todo: ifdef LW_UDP
	  ifdef UL_ION_LTP
 */

#include <netdb.h>
#include <stdbool.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/ip_icmp.h>
#include <netinet/udp.h>
#include <stdlib.h>
#include <unistd.h>//CCaini aggiunta per compilare con write e read al posto di fwrite e fread
//CCaini comment the line below to find unwanted dependency on ltpP.h
#include<netdb.h> //ALB
//#include "ltpP.h"
#include<stdint.h>

#include "../../eclso.h"
#include "eclsaProtocolAdapters.h"

void initEclsoUpperLevel(int argc, char *argv[], EclsoEnvironment *env)
{
	char *port = NULL; //added by LBA
	unsigned long long ownEngineID;


	// TODO CCaini: find a way to get the maxLTPSegmentPayloadLenght value dynamically
	int maxSegmentPayloadLenght=1024;

	//Note that in UniboLTP the value declared in the eclso string (e.g. 1024)
	//is the segment size of the LTP segment payload, not that of the whole LTP segment;
	env->T = sizeof(uint16_t) + MAX_LTP_SEGMENT_HEADER_LENGTH + maxSegmentPayloadLenght;

	//Commentare la riga sotto se voglio debuggare la init e basta facendo partire eclso
	//con la stringa dei parametri Provato ma ho un errore in Eclipse

	//CCaini da passare aggiungendolo alla lista dei parametri come in ION?
    //ownID viene passato da UniboLTP sulla socket 3
	int returned_value = read(3, &ownEngineID, sizeof(ownEngineID));

	/* --- Added by ALB --- */
	if(returned_value <= 0) { // significa che c'è stato un errore in lettura
		exit(1); // TODO: check if already exists a way to signal error
	}
	/* ^^^ Added by ALB ^^^ */

	env->ownEngineId = (unsigned short) ownEngineID; //=Numero Nodo; mettere uint16?


}

void initEclsiUpperLevel(int argc, char *argv[], EclsiEnvironment *env)
{
//now the IP:port parsing is carried out in the LP interface
//In the Unibo-LTP case it does not do nothing (in ION does)
}

void sendSegmentToUpperProtocol(char *buffer,int *bufferLength)
{
	// Write the int for size
	int returned_value = write(3, bufferLength, sizeof(*bufferLength));
	/* --- Added by ALB --- */
	if(returned_value <= 0) { // significa che c'è stato un errore in scrittura
		printf("Error in sendSegmentToUpperProtocol write\n");
		exit(1); // TODO: check if already exists a way to signal error
	}
	/* ^^^ Added by ALB ^^^ */

	returned_value = write(3, buffer, *bufferLength);
	/* --- Added by ALB --- */
	if(returned_value <= 0) { // significa che c'è stato un errore in scrittura
		printf("Error in sendSegmentToUpperProtocol write\n");
		exit(1); // TODO: check if already exists a way to signal error
	}
	/* ^^^ Added by ALB ^^^ */
}

void receiveSegmentFromUpperProtocol(char *buffer,int *bufferLength)
{
	int returned_value = read(3, (void*) bufferLength, sizeof(*bufferLength));
	/* --- Added by ALB --- */
	if(returned_value <= 0) { // significa che c'è stato un errore in lettura
		printf("Error in receiveSegmentFromUpperProtocol read\n");
		exit(1); // TODO: check if already exists a way to signal error
	}
	/* ^^^ Added by ALB ^^^ */

	returned_value = read(3, buffer, *bufferLength);
	/* --- Added by ALB --- */
	if(returned_value <= 0) { // significa che c'è stato un errore in lettura
		printf("Error in receiveSegmentFromUpperProtocol read\n");
		exit(1); // TODO: check if already exists a way to signal error
	}
	/* ^^^ Added by ALB ^^^ */
}



